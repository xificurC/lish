# See LICENSE file for copyright and license details.
include config.mk

SRC = lish.c
OBJ = ${SRC:.c=.o}

all: lish

options:
	@echo lish build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.o:
	$(LD) -o $@ $< $(LDFLAGS)

.c.o:
	$(CC) -c -o $@ $< $(CFLAGS)

lish: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

# ${OBJ}: arg.h

install: all
	mkdir -p ${DESTDIR}${DOCDIR}
	mkdir -p ${DESTDIR}${BINDIR}
	mkdir -p ${DESTDIR}${MAN1DIR}
	install -d ${DESTDIR}${BINDIR} ${DESTDIR}${MAN1DIR}
	install -m 644 CHANGES README FAQ LICENSE ${DESTDIR}${DOCDIR}
	install -m 775 lish ${DESTDIR}${BINDIR}
	sed "s/VERSION/${VERSION}/g" < lish.1 > ${DESTDIR}${MAN1DIR}/lish.1
	chmod 644 ${DESTDIR}${MAN1DIR}/lish.1

uninstall: all
	rm -f ${DESTDIR}${MAN1DIR}/lish.1 \
		${DESTDIR}${BINDIR}/lish
	rm -rf ${DESTDIR}${DOCDIR}

dist: clean
	mkdir -p lish-${VERSION}
	cp -R Makefile CHANGES README FAQ LICENSE strlcpy.c arg.h \
		config.mk lish.c lish.1 lish-${VERSION}
	tar -cf lish-${VERSION}.tar lish-${VERSION}
	gzip lish-${VERSION}.tar
	rm -rf lish-${VERSION}

clean:
	rm -f lish *.o
