

// GLOBAL variable space (functions are variables too)
// no locals, just 1 global place for variables, in the form of key-value pairs.
// a local will be generated with bytecode as
// - reading the current value
// - setting up the new value
// - restoring the previous one

// DATA stack
// (+ 1 2) generates vm_put(1); vm_put(2); vm_plus
// where vm_plus is vm_push(vm_get() + vm_get());

// CALL stack
// calling a function foo does vm_push(*ip)
// exiting a function does jump(vm_pull())

// DATATYPES
// the stack holds 32-bit values (Values) which are free to be interpreted by the user as wished
// - a number is simply pushed on the data stack - vm_put(100)
// no differentiation between signed and unsigned. < is signed less and u< is unsigned less.
// up to the user to decide how to use a number.
// - a Value can be used as an ASCII character by some routines.
// - a string is a pointer into the heap space where a null-terminated string resides.
// In ptr-4 is the length of the string.
// - a cons cell is 2 Values next to each other. On the data stack is a pointer to the start.
// - Nil is a special Value signifying nothingness.
// - a list is a set of cons cells terminated by Nil.
// - an array is a contiguous block of heap space. Array(100) is 100 Values. Pointer on data stack.
// - an object is represented as an array of Values for each underlying piece.
// (object point x y) defines
//   - (point 1 2) - constructor that creates the object, puts it in heap and pointer on data stack
//   - (point-x@)  - function that fetches x from the object
//   - (point-x! 10) - function that overwrites x in the object
//   - point-y@ and point-y! respectively

// COMPILER
// the reader reads lisp source code and generates bytecode.
// The bytecode gets interpreted aferwards.
// Later it will be possible to byte-compile a source file.
// A Word is separated by ()[]{}'@& or space. Words are just strings.
// () - parens represent a list. When the compiler finds an unquoted list and the first Word
//      is not a macro or builtin it is taken to be a variable holding a function pointer.
// [] - brackets create a process object
// '  - quote
// @  - fetch a variable's value. a is "a" and @a is value of a. To make the string quote it - "@a"
// {} - a list of cons cells. '{a 1 b 2} is the same as '((a . 1) (b . 2)) in traditional lisps
// &  - varargs. (set foo (fn (x &xs) (list @x @xs))) (foo 1 2 3) -> (1 (2 3))

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#define VALUE_SIZE 4
#define VALUE_MORE(x) x = x + VALUE_SIZE
#define VALUE_LESS(x) x = x - VALUE_SIZE

typedef int32_t Value;

struct Cons {
        Value *car;
        Value *cdr;
};

struct VM {
        Value *vars;
        Value *data;
        Value *dataNext;
        Value *call;
        Value *heap;
};

void vm_init(struct VM *vm) {
        vm->vars = malloc(256*sizeof(Value));
        vm->data = malloc(256*sizeof(Value));
        vm->dataNext = vm->data;
        vm->call = malloc(256*sizeof(Value));
        vm->heap = malloc(256*sizeof(Value));
}

void vm_free(struct VM *vm) {
        free(vm->vars);
        free(vm->data);
        free(vm->call);
        free(vm->heap);
}

////////////////////////
// DATA stack operations
////////////////////////

Value vm_data_depth(struct VM *vm) {
        return (vm->dataNext - vm->data) / sizeof(Value);
}

Value vm_data_empty(struct VM *vm) {
        return (vm_data_depth(vm) == 0);
}

// puts val into data stack
void vm_put(struct VM *vm, Value val) {
        *(vm->dataNext++) = val;
}

// gets topmost Value from data stack
Value vm_get(struct VM *vm) {
        if (vm_data_empty(vm)) {
                fputs("Internal error: data stack underflow\n", stderr);
                exit(2);
        }
        return *(--vm->dataNext);
}

// sums top two Values and puts the result back in
void vm_plus(struct VM *vm) {
        Value b = vm_get(vm);
        Value a = vm_get(vm);
        vm_put(vm, a + b);
}

void vm_minus(struct VM *vm) {
        Value b = vm_get(vm);
        Value a = vm_get(vm);
        vm_put(vm, a - b);
}

void vm_print_data_stack(struct VM *vm) {
        Value *at = vm->data;
        fputc(' ', stderr);
        while (at < vm->dataNext) {
                fprintf(stderr, "%d ", *at);
                at++;
        }
        fputs(".\n", stderr);
}

int main(/* int argc, char **argv */) {
        struct VM *vm = malloc(sizeof(struct VM));
        vm_init(vm);
        vm_put(vm, 3);
        vm_print_data_stack(vm);
        vm_put(vm, 4);
        vm_print_data_stack(vm);
        vm_plus(vm);
        vm_print_data_stack(vm);
        vm_get(vm);
        vm_print_data_stack(vm);
        vm_free(vm);
        free(vm);
        exit(0);
}
